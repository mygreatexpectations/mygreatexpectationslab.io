FROM node:latest

RUN apt-get update && apt-get upgrade -y
RUN apt-get install apt-transport-https -y
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update && apt-get install yarn tree -y
RUN mkdir /home/src
COPY . /home/src
WORKDIR /home/src 
RUN tree /home
RUN yarn install
RUN tree /home	
RUN yarn run generate 
RUN tree /home
RUN du -sh node_modules

